import {IForm} from '../form/form.component';

class SelectorsController implements ng.IComponentController {
  public availableForms: IForm[];
  public selected: IForm;
  public disableForm: boolean;
  public ngModel: string[];
  public filteredAvailableForms: any = [];

  /**
   * Инициализация компонента
   */
  $onInit() {
    this.filteredAvailableForms = [...this.availableForms];

    if (this.ngModel && this.ngModel.length > 0) {
      this.ngModel.forEach((item) => {
        this.filteredAvailableForms.forEach((itemAvailableForm, i) => {
          if (item === itemAvailableForm.formName) {
            this.filteredAvailableForms.splice(i, 1);
          }
        });
      });
    }

    const requireForm = this.filteredAvailableForms.find((item) => {
      return item.require === true;
    });

    if (requireForm) {
      this.disableForm = true;
      this.selected = requireForm;
      this.ngModel.push(this.selected.formName);
    } else {
      this.selected = this.filteredAvailableForms[0];
      this.ngModel.push(this.selected.formName);
    }
  }

  /**
   * Изменение элемента в списке
   */
  changeSelected() {
    this.ngModel.pop();
    this.ngModel.push(this.selected.formName);
  }
}

export class SelectorsComponent implements ng.IComponentOptions {
  controller: ng.IControllerConstructor;
  template: string;
  bindings: any;

  constructor() {
    this.bindings = {
      availableForms: '<',
      ngModel: '='
    };
    this.controller = SelectorsController;
    this.template = require('./selectors.component.html');
  }
}

