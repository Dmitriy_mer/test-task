import * as angular from 'angular';
import Components from '../components.module';

describe('SelectorsComponent', () => {
  beforeEach(angular.mock.module(Components));

  beforeEach(inject(($injector, $compile) => {
    this.$scope = $injector.get('$rootScope');

    this.$scope.availableForms = [
      {
        formName: 'One',
        require: true
      },
      {
        formName: 'Two',
        require: true
      }
    ];

    this.$scope.selectedForms = [];

    this.element = $compile(
      `<selectors
         ng-model="selectedForms"
         available-forms="availableForms">
       </selectors>`
    )(this.$scope);

    this.$ctrl = this.element.controller('selectors');
    this.$scope.$digest();
  }));

  it('should be created', () => {
    expect(this.$ctrl).toBeTruthy();
  });

  it('available forms are defined', () => {
    expect(this.$ctrl.availableForms).toEqual(this.$scope.availableForms);
  });

  it('selected forms are empty', () => {
    this.$scope.selectedForms = [];
    this.$scope.$digest();
    expect(this.$ctrl.ngModel).toEqual([]);
  });

  it('selected forms has one', () => {
    this.$scope.$digest();
    expect(this.$ctrl.ngModel).toEqual(['One']);
  });

  it('filtered forms has only one object', () => {
    this.$scope.$digest();
    this.$ctrl.$onInit();
    expect(this.$ctrl.filteredAvailableForms.length).toBe(1);
  });
});
