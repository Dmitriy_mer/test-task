import * as angular from 'angular';
import Components from '../components.module';

describe('FormComponent', () => {
  beforeEach(angular.mock.module(Components));

  beforeEach(inject(($injector, $compile) => {
    this.$scope = $injector.get('$rootScope');

    this.$scope.selectedForms = [];

    this.element = $compile(
      `<form-component>
       </form-component>`
    )(this.$scope);

    this.$ctrl = this.element.controller('formComponent');
    this.$scope.$digest();
  }));

  it('should be created', () => {
    expect(this.$ctrl).toBeTruthy();
  });

  it('available forms are defined', () => {
    expect(this.$ctrl.availableForms.length).toBe(5);
  });

  it('it call add component function from interface', () => {
    spyOn(this.$ctrl, 'onAddComponent').and.callThrough();
    this.element.find('.spec-add-button').click();

    expect(this.$ctrl.onAddComponent).toHaveBeenCalled();
  });

  it('it call delete component function from interface', () => {
    this.$ctrl.onAddComponent();
    spyOn(this.$ctrl, 'onDeleteComponent').and.returnValues([]);
    this.element.find('.spec-delete-button').click();

    expect(this.$ctrl.onDeleteComponent).toHaveBeenCalled();
  });

  it('add button is disabled', () => {
    this.$ctrl.selectedForms = [...this.$ctrl.availableForms];
    expect(this.$ctrl.isDisabledAddButton).toBeTruthy();
  });

  it('delete button is disabled', () => {
    expect(this.$ctrl.isDisabledDeleteButton).toBeTruthy();
  });

  it('add button is enabled', () => {
    expect(this.$ctrl.isDisabledAddButton).toBeFalsy();
  });

  it('delete button is enabled', () => {
    this.$ctrl.selectedForms = [...this.$ctrl.availableForms];
    expect(this.$ctrl.isDisabledDeleteButton).toBeFalsy();
  });

});
