import * as angular from 'angular';

export interface IForm {
  formName: string;
  require: boolean;
}

class FormController implements ng.IComponentController {
  public selectedForms: string[] = [];
  public availableForms: IForm[] = [
    {
      formName: 'One',
      require: false
    },
    {
      formName: 'Two',
      require: true
    },
    {
      formName: 'Three',
      require: false
    },
    {
      formName: 'Four',
      require: true
    },
    {
      formName: 'Five',
      require: false
    }
  ];
  private $element: any;
  private $scope: any;
  private $compile: any;
  private countRequireForms = 0;

  constructor($element, $scope, $compile) {
    'ngInject';
    this.$element = $element;
    this.$scope = $scope;
    this.$compile = $compile;

    this.availableForms.forEach((item) => {
      if (item.require) {
        return this.countRequireForms++;
      }
    });

  }

  /**
   * Добавление компонента на форму
   */
  onAddComponent() {
    const element = angular.element('<selectors>')
      .attr({
        'ng-model': '$ctrl.selectedForms',
        'available-forms': '$ctrl.availableForms'
      });
    const componentScope = this.$scope.$new();
    this.$element.append(element);
    this.$compile(element)(componentScope);
  }

  /**
   * Удаление компонента с формы
   */
  onDeleteComponent() {
    console.log('i am here');
    const elements = angular.element(document.querySelectorAll('selectors'));
    elements[elements.length - 1].remove();
    this.selectedForms.pop();
  }

  /**
   * Проверяет, можно ли добавить элементы на форму
   */
  get isDisabledAddButton() {
    if (this.selectedForms.length === this.availableForms.length) {
      return true;
    }
  }

  /**
   * Проверяет, можно ли удалять элементы с формы
   */
  get isDisabledDeleteButton() {
    if (this.selectedForms.length > 0 && this.selectedForms.length > this.countRequireForms) {
      return false;
    }
    return true;
  }
}

export class FormComponent implements ng.IComponentOptions {
  controller: ng.IControllerConstructor;
  template: string;

  constructor() {
    this.controller = FormController;
    this.template = require('./form.component.html');
  }
}


