import * as angular from 'angular';

import {FormComponent} from './form/form.component';
import {SelectorsComponent} from './selectors/selectors.component';


const Components: ng.IModule = angular
  .module('app.components', [])
  .component('formComponent', new FormComponent())
  .component('selectors', new SelectorsComponent());

export default Components.name;
