import * as angular from 'angular';
import { AppComponent } from './app.component';
import Components from './components/components.module';

// import './app.scss';

const App: ng.IModule = angular
  .module('app', [
    Components
  ])
  .component('app', new AppComponent());

export default App.name;
